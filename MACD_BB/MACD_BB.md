
Continue read from https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/page-51


SPX setup
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/post-56303

Amazon setup
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/post-48254



Note:

For SPX only - There is one thing I'm noticing with all the double and triple backtesting, as well as live:
If the red dots come down (above the zero line coming down to the zero line) and actually touch the zero line...but doesn't cross (basically "bounces" off of it) a call opportunity has shown itself, as it always has a tendency to reverse. The same applies for a put...if the white dots are coming up to the zero line (coming from bottom to top) and it bounces off the bottom of the zero line....a put opportunity is there. Take a look at this screenshot I have attached for an example from this morning...SPX went up $6 once that white dot appeared. I've seen a lot of these same type scenarios. Backtest it to feel good about it tho.
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/post-48477

San777 said:
@Hypoluxa, this very interesting, been keeping close eye on it, Do you wait till second white or red dot appear above or below line for entry ?
yes, because that shows a good, clear, gap.
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/post-48880

On Mondays and Tuesdays, I use Wednesday. On Wednesday and Thursdays I choose Friday and on Fridays I use the next Monday.
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/post-54145


Use CamarilaCalculator
    https://www.pivottrading.co.in/pages/advancedCamarilla.php
    or
    https://www.earnforex.com/pivot-points-calculator/

Gank Calculator
    https://www.stockmaniacs.net/freebies/free-tools/gann-square-of-9-calculator/

Use VIX as help
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/post-56790


Dynamic Hiding Pivots
    https://usethinkscript.com/threads/spx-trading-strategy-for-thinkorswim.5087/post-57995


Sample of 
https://content.screencast.com/users/RichBlow/folders/Capture/media/a613ea18-b9c0-4d03-99ec-472364cb72a5/LWR_Recording.png
https://content.screencast.com/users/RichBlow/folders/Capture/media/347f7fa8-f415-4560-bb80-50970c163e84/LWR_Recording.png
https://usethinkscript.com/threads/accumulation-swing-index-mtf-indicator-for-thinkorswim.5245/
