# Double Bottom 
# Mobius 

# input
input displace = 0; # find out what it is
input lengthShort = 20; # the short moving average value
input pctAbove = 5.0; # percent value above the short moving average value
input pctBelow = 3.0; # percent value below the short moving average value
input lengthLong = 200; # the long moving average value
input priceType = close;
input priceBelow = FundamentalType.CLOSE;
input aggregationPeriod = AggregationPeriod.DAY;
input averageType = AverageType.SIMPLE;
input fundamentalType = FundamentalType.CLOSE;
 
# MovAvgEnvelope Begin

plot UpperBand = MovingAverage(averageType, priceType[-displace], lengthShort) * (1 + pctAbove / 100);
UpperBand.SetDefaultColor(GetColor(0));

plot LowerBand = MovingAverage(averageType, priceType[-displace], lengthShort) * (1 - pctBelow / 100);
lowerBand.SetDefaultColor(GetColor(0));

# DailySMAShort

plot DailySMAShort;
if !IsNaN(close(period = aggregationPeriod)[-1]) {
    DailySMAShort = Double.NaN;
} else {
    DailySMAShort = Average(fundamental(fundamentalType, period = aggregationPeriod)[-displace], lengthShort);
}

DailySMAShort.SetDefaultColor(GetColor(2));
DailySMAShort.SetPaintingStrategy(PaintingStrategy.HORIZONTAL);

# DailySMA Start

plot DailySMALong;

if !IsNaN(close(period = aggregationPeriod)[-1]) {
    DailySMALong = Double.NaN;
} else {
    DailySMALong = Average(fundamental(fundamentalType, period = aggregationPeriod)[-displace], lengthLong);
}

DailySMALong.SetDefaultColor(GetColor(1));
DailySMALong.SetPaintingStrategy(PaintingStrategy.HORIZONTAL);

# Mobius Double Bottom

input Percent_A_to_B = 2; 
input Percent_A_to_C = .1; 
input nP = 13; 
input ShowValues = yes; 
input ShowLines = yes; 

def AB = Percent_A_to_B / 100; 
def AC = Percent_A_to_C / 100; 
def h = high; 
def l = low; 
def bar = BarNumber();  # Returns the current bar number.
def Ps = 1 + AC; 
def Ns = 1 - AC;  
def ll = fold j = 1 to nP + 1  
         with q = 1 
         while q 
         do l < GetValue(low, -j); 
def PivotL = if (bar > nP and  
                 l == Lowest(l, nP) and  
                 ll)  
            then l  
            else Double.NaN; 
def PL1 = if !isNaN(PivotL)  
          then l  
          else PL1[1]; 
def PL1Bar = if !IsNaN(PivotL)  
             then bar  
             else PL1Bar[1]; 
 
def PL2 = if PL1 != PL1[1] 
          then PL1[1] 
          else PL2[1]; 
def PL2Bar = if PL1Bar != PL1Bar[1] 
             then Pl1Bar[1]  
             else PL2Bar[1]; 

def PL3 = if PL2 != PL2[1] 
          then PL2[1] 
          else PL3[1]; 
def PL3Bar = if PL2Bar != PL2Bar[1] 
             then Pl2Bar[1]  
             else PL3Bar[1]; 
def Check2 = if between(PL3, PL2 * Ns, PL2 * Ps) 
             then 1 
             else 0; 
def PL4 = if PL3 != PL3[1] 
          then PL3[1] 
          else PL4[1]; 
def PL4Bar = if PL3Bar != PL3Bar[1] 
             then PL3Bar[1] 
             else PL4Bar[1]; 
def Check3 = if between(PL4, PL3 * Ns, PL3 * Ps) 
             then 1 
             else 0; 
def PL5 = if PL4 != PL4[1] 
          then PL4[1] 
          else PL5[1]; 
def PL5Bar = if PL4Bar != PL4Bar[1] 
             then PL4Bar[1] 
             else PL5Bar[1]; 
def Check4 = if between(PL5, PL4 * Ns, PL4 * Ps) 
             then 1 
             else double.nan; 
 
plot pivotLow = PivotL; 
     pivotLow.SetDefaultColor(Color.Yellow); 
     pivotLow.SetPaintingStrategy(PaintingStrategy.VALUES_BELOW); 
     pivotLow.SetHiding(!ShowValues); 
plot PivotDot = PivotL; 
 
 
 
     PivotDot.SetDefaultColor(Color.Gray); 
     PivotDot.SetPaintingStrategy(PaintingStrategy.POINTS); 
     PivotDot.SetLineWeight(3); 
 
# End Code 