input numDevDn = -2.0;
input numDevUp = 2.0;
input timeFrame = {default DAY, WEEK, MONTH};
input time = yes;
input time_color = {default "magenta", "green", "pink", "cyan", "orange", "red", "blue", "gray", "violet"};
input VWAP_label = yes;
input VWAP_Color = {default "magenta", "green", "pink", "cyan", "orange", "red", "blue", "gray", "violet"};
input Upperbands_label = Yes;
input UpperBand_Color = {default "magenta", "green", "pink", "cyan", "orange", "red", "blue", "gray", "violet"};
input Lowerbands_label = Yes;
input LowerBand_color = {default "magenta", "green", "pink", "cyan", "orange", "red", "blue", "gray", "violet"};

def cap = getAggregationPeriod();
def errorInAggregation =
    timeFrame == timeFrame.DAY and cap >= AggregationPeriod.WEEK or
    timeFrame == timeFrame.WEEK and cap >= AggregationPeriod.MONTH;
assert(!errorInAggregation, "timeFrame should be not less than current chart aggregation period");

def yyyyMmDd = getYyyyMmDd();
def periodIndx;
switch (timeFrame) {
case DAY:
    periodIndx = yyyyMmDd;
case WEEK:
    periodIndx = Floor((daysFromDate(first(yyyyMmDd)) + getDayOfWeek(first(yyyyMmDd))) / 7);
case MONTH:
    periodIndx = roundDown(yyyyMmDd / 100, 0);
}
def isPeriodRolled = compoundValue(1, periodIndx != periodIndx[1], yes);

def volumeSum;
def volumeVwapSum;
def volumeVwap2Sum;

if (isPeriodRolled) {
    volumeSum = volume;
    volumeVwapSum = volume * vwap;
    volumeVwap2Sum = volume * Sqr(vwap);
} else {
    volumeSum = compoundValue(1, volumeSum[1] + volume, volume);
    volumeVwapSum = compoundValue(1, volumeVwapSum[1] + volume * vwap, volume * vwap);
    volumeVwap2Sum = compoundValue(1, volumeVwap2Sum[1] + volume * Sqr(vwap), volume * Sqr(vwap));
}
def price = volumeVwapSum / volumeSum;
def deviation = Sqrt(Max(volumeVwap2Sum / volumeSum - Sqr(price), 0));

plot VWAP = price;

plot UpperBand = price + numDevUp * deviation;
UpperBand.hide();
plot LowerBand = price + numDevDn * deviation;
LowerBand.hide();


vwap.DefineColor("UP", Color.GREEN);
vwap.DefineColor("DOWN", Color.LIGHT_RED);
vwap.AssignValueColor( if close> vwap then vwap.Color("UP") else vwap.Color("DOWN"));
UpperBand.setDefaultColor(getColor(2));
LowerBand.setDefaultColor(getColor(4));

AddLabel(time,  "Daily", GetColor(time_Color));
AddLabel(VWAP_label, " VWAP = " + Round(VWAP, 2), GetColor(VWAP_Color));
AddLabel(Upperbands_label, " VWAP UpperBand = " + round(UpperBand, 2), GetColor(UpperBand_Color));
AddLabel(Lowerbands_label, " VWAP LowerBand = " + round(LowerBand, 2), GetColor(LowerBand_Color));