#https://usethinkscript.com/threads/advanced-volume-indicator-for-thinkorswim.1665/

#welkincall@gmail.com
#v4.23.2020
input AvgDayVolLength = 5;
input AvgVolLength = 20;
input ShowDayVolLabel = yes;
input ShowBarVolLabel = yes;
input ShowBuySellStrength = yes;
input ShowEthTotalVol = yes;
input BuySellStrAggregation2 = AggregationPeriod.THIRTY_MIN;
input VolAverageType = AverageType.SIMPLE;

def NA = Double.NaN;
def PriceRange = high - low;
def DayVol = volume("period" = AggregationPeriod.DAY);
def AvgDayVol = Average(DayVol,AvgDayVolLength);
def Start = 0930;
def End = 1600;
def conf = SecondsFromTime(Start) >= 0 and SecondsFromTime(End) <= 0;


plot VolColor = NA;
VolColor.DefineColor("Bullish", Color.GREEN);
VolColor.DefineColor("Bearish", Color.RED);
VolColor.DefineColor("VolAvg", CreateColor(0,100,200));
VolColor.DefineColor("VolSigma2", Color.DARK_ORANGE);
VolColor.DefineColor("VolSigma3", Color.Magenta);
VolColor.DefineColor("Relative to Prev", Color.YELLOW);
VolColor.DefineColor("Below Average", Color.GRAY);
VolColor.DefineColor("ETH TVOL", Color.GRAY);

def Vol = volume;
def VolAvg = MovingAverage(VolAverageType, Volume, AvgVolLength);

#2Sigma and 3Sigma Vol Filter
def Num_Dev1 = 2.0;
def Num_Dev2 = 3.0;
def averageType = AverageType.SIMPLE;
def sDev = StDev(data = Vol, length = AvgVolLength);

def VolSigma2 = VolAvg + Num_Dev1 * sDev;
def VolSigma3 = VolAvg + Num_Dev2 * sDev;

#Current Candle Buy and Sell Strength
def BuyStr = ((close - low)/PriceRange)*100;
def SellStr = ((high - close)/PriceRange)*100;


def BuyStr2 = ((close("period"=BuySellStrAggregation2) - low("period"=BuySellStrAggregation2))/(high("period"=BuySellStrAggregation2) - low("period"=BuySellStrAggregation2)))*100;
def SellStr2 = ((high("period"=BuySellStrAggregation2) - close("period"=BuySellStrAggregation2))/(high("period"=BuySellStrAggregation2) - low("period"=BuySellStrAggregation2)))*100;


def RelDayVol = DayVol / AvgDayVol;
def RelPrevDayVol = DayVol / volume("period" = AggregationPeriod.DAY)[1];
#Daily aggregation volume labels
AddLabel(if GetAggregationPeriod() >= AggregationPeriod.DAY then 0 else if ShowDayVolLabel then 1 else 0, "DayVol: " + DayVol +" / "+ Round(RelDayVol,2) + "x Avg("+ AvgDayVolLength +") / "+ Round(RelPrevDayVol,2) +"x Prev", if DayVol > AvgDayVol then VolColor.Color("VolAvg") else VolColor.Color("Below Average"));

def RelVol = Vol/VolAvg;
def RelPrevVol = volume / volume[1];

#current aggregation's volume labels
AddLabel(ShowBarVolLabel, "Vol: " + volume +" / "+ Round(RelVol,2) + "x Avg("+ AvgVolLength +") / "+ Round(RelPrevVol,2)+"x Prev",  if vol > VolSigma3 then VolColor.Color("VolSigma3") else if Vol > VolSigma2 then VolColor.Color("VolSigma2") else if Vol > VolAvg then VolColor.Color("VolAvg") else VolColor.Color("Below Average"));

#ETH Total Vol
def ETH_VOL = if !conf and conf[1] then volume else if !conf then CompoundValue(1, ETH_VOL[1] + volume, volume) else ETH_VOL[1];

AddLabel(if !ShowEthTotalVol then 0 else if GetAggregationPeriod() >= AggregationPeriod.DAY then 0 else 1, "ETH TVOL: " + ETH_VOL, VolColor.Color("ETH TVOL"));

#current candle Buy/Sell strength labels
AddLabel(if ShowBuySellStrength then 1 else 0, " ", Color.BLACK);
AddLabel(if ShowBuySellStrength then 1 else 0, "1", Color.GRAY);
AddLabel(if ShowBuySellStrength then 1 else 0, "Sell "+Round(SellStr,2) +"%", if SellStr > BuyStr then Color.RED else Color.DARK_RED);
AddLabel(if ShowBuySellStrength then 1 else 0, "Buy "+Round(BuyStr,2) +"%", if BuyStr > SellStr then Color.GREEN else Color.DARK_GREEN);

#2nd Aggregation Buy/Sell strength labels
AddLabel(if GetAggregationPeriod() >= BuySellStrAggregation2 or !ShowBuySellStrength then 0 else 1, " ", Color.BLACK);
AddLabel(if GetAggregationPeriod() >= BuySellStrAggregation2 or !ShowBuySellStrength then 0 else 1, "2", Color.GRAY);
AddLabel(if GetAggregationPeriod() >= BuySellStrAggregation2 or !ShowBuySellStrength then 0 else 1, "Sell "+Round(SellStr2,2) +"%", if SellStr2 > BuyStr2 then Color.RED else Color.DARK_RED);
AddLabel(if GetAggregationPeriod() >= BuySellStrAggregation2 or !ShowBuySellStrength then 0 else 1, "Buy "+Round(BuyStr2,2) +"%", if BuyStr2 > SellStr2 then Color.GREEN else Color.DARK_GREEN);