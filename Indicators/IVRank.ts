## **TS Name** ##
# IVRank
## **Origin** ##
#* Got it from Steve
## **Scope** ##
#* It displays the IVRank 
#
#    - Above 50 is GREEN
#    - Neutral is YELLOW 
#    - Below 25 is RED 
#

#start
input TimePeriod = 252;
input DisplayIVPercentile = yes;
def vol = imp_volatility(period = AggregationPeriod.DAY);
def data = if !IsNaN(vol) then vol else vol[-1];
def hi = Highest(data, TimePeriod);
def lo = Lowest(data, TimePeriod);
plot IVRank = (data - lo) / (hi - lo) * 100;
IVRank.Hide();
def lowend = IVRank < 25;
def highend = IVRank > 50;
AddLabel(DisplayIVPercentile , Concat("IV Rank: ", AsPercent(IVRank / 100))
    , if lowend then Color.RED else if highend then Color.green else if Double.NaN then Color.red else Color.YELLOW);
#end
