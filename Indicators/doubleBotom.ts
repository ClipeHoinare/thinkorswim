script GetLowestValueFlagged {
    input barNumber = 0;
    input lookBack = 0;
    input lowPrice = 0;

    def ret_val = if (barNumber > lookBack and  
                 lowPrice == Lowest(low, lookBack) ) 
            then lowPrice  
            else Double.NaN;

    plot return = ret_val;
}

script GetHighestValueFlagged {
    input barNumber = 0;
    input lookBack = 0;
    input highPrice = 0;

#plot ClosingPriceForHighestHigh = GetValue(close, GetMaxValueOffset(high, 12), 12);

    def ret_val = if (barNumber > lookBack and  
                 highPrice == Highest(high, lookBack))  
            then highPrice  
            else Double.NaN;

    plot return = ret_val;
}

script PlotDot {
    input PivotL = 0;

    plot pivotLow = PivotL;
    pivotLow.SetDefaultColor(Color.YELLOW);
    pivotLow.SetPaintingStrategy(PaintingStrategy.VALUES_BELOW);

    plot PivotDot = PivotL;
    PivotDot.SetDefaultColor(Color.GRAY);
    PivotDot.SetPaintingStrategy(PaintingStrategy.POINTS);
    PivotDot.SetLineWeight(5);

    plot return = pivotLow;

}

# Double Bottom 
# Mobius 

# input
input lengthShort = 20; # the short moving average value
input pctAbove = 5.0; # percent value above the short moving average value
input pctBelow = 3.0; # percent value below the short moving average value
input lengthLong = 200; # the long moving average value
input lookBack = 20; # look back for nn days 
input lookJiggle = 5; # jiggle n days around the lookBack value

input aggregationPeriod = AggregationPeriod.DAY;
input averageType = AverageType.SIMPLE;
input priceType = close;
input fundamentalType = FundamentalType.CLOSE;
input displace = 0; # find out what it is
 
# ShortBand
plot ShortBand;
ShortBand = Average(Fundamental(fundamentalType, period = aggregationPeriod)[-displace], lengthShort);

ShortBand.SetDefaultColor(Color.VIOLET);
ShortBand.SetPaintingStrategy(PaintingStrategy.HORIZONTAL);

# MovAvgEnvelope Above
plot UpperBand = MovingAverage(averageType, priceType[-displace], lengthShort) * (1 + pctAbove / 100);
UpperBand.SetDefaultColor(Color.RED);

# MovAvgEnvelope Below
plot LowerBand = MovingAverage(averageType, priceType[-displace], lengthShort) * (1 - pctBelow / 100);
LowerBand.SetDefaultColor(Color.RED);

# DailySMA Start
plot LongBand;
LongBand = Average(Fundamental(fundamentalType, period = aggregationPeriod)[-displace], lengthLong);

LongBand.SetDefaultColor(Color.CYAN);
LongBand.SetPaintingStrategy(PaintingStrategy.HORIZONTAL);


# Double Bottom/Top
def lowPrice = low;
def highPrice = high;
def barNumber = BarNumber();  # Returns the current bar number.

#region plot lowest point
def PivotL = GetLowestValueFlagged(barNumber, lookBack, lowPrice);

#plot pivotLow = PivotL;
#pivotLow.SetDefaultColor(Color.YELLOW);
#pivotLow.SetPaintingStrategy(PaintingStrategy.VALUES_BELOW);

plot PivotDot = PivotL;
PivotDot.SetDefaultColor(Color.GRAY);
PivotDot.SetPaintingStrategy(PaintingStrategy.POINTS);
PivotDot.SetLineWeight(5);

#region plot higherst point
def PivotH = GetHighestValueFlagged(barNumber, lookBack, highPrice);

#plot textHigh = PivotH;
#textHigh.SetDefaultColor(Color.YELLOW);
#textHigh.SetPaintingStrategy(PaintingStrategy.VALUES_ABOVE);

plot dotHigh = PivotH;
dotHigh.SetDefaultColor(Color.GRAY);
dotHigh.SetPaintingStrategy(PaintingStrategy.POINTS);
dotHigh.SetLineWeight(5);

#plot ClosingPriceForHighestHigh = GetValue(close, GetMaxValueOffset(high, 12), 12);
#ClosingPriceForHighestHigh.SetDefaultColor(Color.ORANGE);
#ClosingPriceForHighestHigh.SetLineWeight(5);


# 3m code
# look at the price at lookBack ( with a jiggle of lookJiggle - not yet) 
#        and identify if the price was under LowerBand first date
#        and identify if the price is under LowerBand today




def priceStartValue = GetValue(low, lookBack);
def lowerStartValue = LowerBand[lookBack];

def priceEndValue = low;
def lowerEndValue = LowerBand[0];

def priceStart;
def lowerStart;
if  priceStartValue < lowerStartValue
    and priceEndValue < lowerEndValue
{
    priceStart = priceStartValue;
    lowerStart = lowerStartValue;
}
else
{
    priceStart = Double.NaN;
    lowerStart = Double.NaN;
}

def jiggleValue;
jiggleValue = fold looper = 0 to lookJiggle * 2 - 1 
    with tP = Double.NaN while IsNaN(tP) 
    do 
        if GetValue(low, lookBack - lookJiggle + looper) < LowerBand[lookBack - lookJiggle + looper]
            then looper 
        else Double.NaN;


plot textlowerStart = lowerStart;
textlowerStart.SetDefaultColor(Color.RED);
textlowerStart.SetPaintingStrategy(PaintingStrategy.VALUES_ABOVE);

plot textpriceStart = priceStart;
textpriceStart.SetDefaultColor(Color.BLUE);
textpriceStart.SetPaintingStrategy(PaintingStrategy.VALUES_BELOW);

# End Code 

