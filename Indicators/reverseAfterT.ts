####reverseAfterT
##Based on Matt idea that if the stock builds a T at the end of the day after a couple of up days, and it is down, then next day it will start at a higher price. 
##Last Update: 08/06/21

input daysToLookBack = 0; # we are using this for scanner to look for the ones that happen last couple of days
input shadowSize = 0.37; # how big the lower shadow is versus the whole candle

# gathering info about the last 3 candles
def candleUp1 = close[daysToLookBack+1] > open[daysToLookBack+1];
def candleUp2 = close[daysToLookBack+2] > open[daysToLookBack+2];
def candleUp3 = close[daysToLookBack+3] > open[daysToLookBack+3];
def candleUp1O2 = close[daysToLookBack+1] > close[daysToLookBack+2];
def candleUp2O3 = close[daysToLookBack+2] > close[daysToLookBack+3];
def candleUp1R = high[daysToLookBack+1] - close[daysToLookBack+1] < (close[daysToLookBack+1] - low[daysToLookBack+1]) * 1.6;

def trendUp = candleUp1 and candleUp1R and candleUp2 and candleUp3 and candleUp1O2 and candleUp2O3;

# gathering information about current candle
def closeDown = (close[daysToLookBack] < close[daysToLookBack+1]);
def candleDown = (close[daysToLookBack] < open[daysToLookBack]);
def candleT = ((high[daysToLookBack] - low[daysToLookBack]) * shadowSize < (close[daysToLookBack] - low[daysToLookBack]));

# identify if we should buy or not
def arrowBuy = trendUp and closeDown and candleDown and candleT;

#Plot arrows to flag the buy
plot up = if arrowBuy then low - (3 * TickSize()) else Double.NaN;
up.SetPaintingStrategy(PaintingStrategy.ARROW_UP);
up.SetDefaultColor(Color.MAGENTA);

