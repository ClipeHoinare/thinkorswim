## **TS Name** ##
# MWD
## **Origin** ##
#* Got it from KasuSista on 20210416
## **Scope** ##
#* It displays colored labels for Month, Week, Day 
#
#    - Bullish is GREEN 
#    - Above 50 Zone is LIGHT_GREEN
#    - Neutral Zone is YELLOW 
#    - Bearish is RED 
#    - Below 50 Zone is PINK
#

#TOS Indicators

#Update notes:
# Author: Kasu Sita
# 1/29/2021 - Update
# Added price High and Low to test when a candle is completely above the moving average

# Flag to set the candle color on or off
input coloredCandlesOn = yes;

# The moving aveaarges are supported. A 20 period, a 50 period and a 200 period. These can be changed to support any period. 

# Please be aware that for higher aggreagtes, there may not be enough data in the specific instrucment to support the number of periods required. For example, on monthly chart, there may not enough data for a 200 period average for newer stocks.

input lengthShort = 20;
input lengthMedium = 50;
input lengthLong = 200;

# The indicator by default is programer for simple moving avearges. This can be changed to  support any specific srategy.
input averageType = AverageType.SIMPLE;

# The following variables define the closing price.
def priceMonthClose;
def priceWeekClose;
def priceDayClose;

# The following  prices define the low price.
def priceMonthLow;
def priceWeekLow;
def priceDayLow;

# The following prices define the high price
def priceMonthHigh;
def priceWeekHigh;
def priceDayHigh;

def monthShortClose;
def monthMediumClose;
def monthLongClose;

def monthShortHigh;
def monthMediumHigh;
def monthLongHigh;

def monthShortLow;
def monthMediumLow;
def monthLongLow;


plot monthBullishSignal;
plot monthBearishSignal;
plot monthAbove20Signal;
plot monthBelow20Signal;
plot monthNeutralSignal;


plot monthBullishZone;
plot monthBearishZone;
plot monthAbove20Zone;
plot monthBelow20Zone;
plot monthNeutralZone;

# Monthly moving average signal. For a bullish signal check to see if the monthly candle's low is above the monthly moving average. For a bearish signal check to see if the monthly candle's high is lower than the moving average.

if GetAggregationPeriod() <= AggregationPeriod.MONTH {

    # Initialize the price variables for aggregation period
    priceMonthClose = close(period = "Month");
    priceMonthHigh = high(period = "Month");
    priceMonthLow = low(period = "Month");
        
    monthShortClose = MovingAverage(averageType, priceMonthClose, lengthShort);
    monthMediumClose = MovingAverage(averageType, priceMonthClose, lengthMedium);
    monthLongClose = MovingAverage(averageType, priceMonthClose, lengthLong);
    
    monthShortHigh = MovingAverage(averageType, priceMonthHigh, lengthShort);
    monthMediumHigh = MovingAverage(averageType, priceMonthHigh, lengthMedium);
    monthLongHigh = MovingAverage(averageType, priceMonthHigh, lengthLong);
    
    monthShortLow = MovingAverage(averageType, priceMonthLow, lengthShort);
    monthMediumLow = MovingAverage(averageType, priceMonthLow, lengthMedium);
    monthLongLow = MovingAverage(averageType, priceMonthLow, lengthLong);
   
    monthBullishSignal = priceMonthLow > monthShortClose AND priceMonthLow >  monthMediumClose AND priceMonthLow > monthLongClose;
    monthBearishSignal = priceMonthHigh < monthShortClose AND priceMonthHigh < monthMediumClose AND priceMonthHigh < monthLongClose;
    monthBelow20Signal = priceMonthHigh < monthShortClose and !monthBearishSignal;
    monthAbove20Signal = priceMonthLow > monthShortClose and !monthBullishSignal;
    monthNeutralSignal = !monthBullishSignal and !monthBearishSignal and !monthBelow20Signal and !monthAbove20Signal;


    monthBullishZone = monthBullishSignal ;
    monthBearishZone = monthBearishSignal;
    monthBelow20Zone = monthBelow20Signal;
    monthAbove20Zone = monthAbove20Signal;
    monthNeutralZone = monthNeutralSignal;

}
else {
    priceMonthClose = 0;
    priceMonthHigh = 0;
    priceMonthLow = 0;
    
    monthShortClose = 0;
    monthMediumClose = 0;
    monthLongClose = 0;

    monthShortLow = 0;
    monthMediumLow = 0;
    monthLongLow = 0;

    monthShortHigh = 0;
    monthMediumHigh = 0;
    monthLongHigh = 0;

    monthBullishSignal = 0;
    monthBearishSignal = 0;
    monthBelow20Signal = 0;
    monthAbove20Signal = 0;
    monthNeutralSignal=0;

    monthBullishZone = 0;
    monthBearishZone = 0;
    monthBelow20Zone = 0;
    monthAbove20Zone = 0;
    monthNeutralZone = 0;
}

    AddLabel(monthBullishZone, "M", color.GREEN);
    AddLabel(monthBearishZone, "M", Color.RED);
    AddLabel(monthBelow20Zone, "M", Color.PINK);
    AddLabel(monthAbove20Zone, "M", color.LIGHT_GREEN);
    AddLabel(monthNeutralZone, "M", Color.YELLOW);

AssignPriceColor(
if      (coloredCandlesOn and monthBullishZone) then Color.GREEN 
else if (coloredCandlesOn and monthBearishZone) then Color.RED 
else if (coloredCandlesOn and monthBelow20Zone) then Color.PINK
else if (coloredCandlesOn and monthAbove20Zone) then color.LIGHT_GREEN
else if (coloredCandlesOn and monthNeutralZone) then Color.YELLOW 
else Color.CURRENT);

# Weekly averages

def weekShortClose;
def weekMediumClose;
def weekLongClose;

def weekShortHigh;
def weekMediumHigh;
def weekLongHigh;

def weekShortLow;
def weekMediumLow;
def weekLongLow;


plot weekBullishSignal;
plot weekBearishSignal;
plot weekBelow50Signal;
plot weekAbove50Signal;
plot weekNeutralSignal;

plot weekBullishZone;
plot weekBearishZone;
plot weekBelow50Zone;
plot weekAbove50Zone;
plot weekNeutralZone;


    if GetAggregationPeriod() <= AggregationPeriod.WEEK {
        priceWeekClose = close(period = "week");
        priceWeekHigh = high(period = "week");
        priceWeekLow = low(period = "week");
        
        weekShortClose = MovingAverage(averageType, priceWeekClose, lengthShort);
        weekMediumClose = MovingAverage(averageType, priceWeekClose, lengthMedium);
        weekLongClose = MovingAverage(averageType, priceWeekClose, lengthLong);

        weekShortHigh = MovingAverage(averageType, priceWeekHigh, lengthShort);
        weekMediumHigh = MovingAverage(averageType, priceWeekHigh, lengthMedium);
        weekLongHigh = MovingAverage(averageType, priceWeekHigh, lengthLong);

        weekShortLow = MovingAverage(averageType, priceWeekLow, lengthShort);
        weekMediumLow = MovingAverage(averageType, priceWeekLow, lengthMedium);
        weekLongLow = MovingAverage(averageType, priceWeekLow, lengthLong);

        weekBullishSignal = priceWeekLow > weekShortClose AND priceWeekLow > weekMediumClose AND priceWeekLow > weekLongClose;
        weekBearishSignal = priceWeekHigh < weekShortClose AND priceWeekHigh < weekMediumClose AND priceWeekHigh < weekLongClose;    
        weekBelow50Signal = priceWeekHigh < weekMediumClose and !weekBearishSignal;
        weekAbove50Signal = priceWeekLow > weekMediumClose and !weekBullishSignal;
        weekNeutralSignal = !weekBullishSignal and !weekBearishSignal and !weekBelow50Signal and !weekAbove50Signal;

        weekBullishZone = weekBullishSignal;
        weekBearishZone = weekBearishSignal;
        weekBelow50Zone = weekBelow50Signal;
        weekAbove50Zone = weekAbove50Signal;
        weekNeutralZone = weekNeutralSignal;

    }
    else {

        priceWeekClose = 0;
        priceWeekHigh = 0;
        priceWeekLow = 0;
    
        weekShortClose=0;
        weekMediumClose=0;
        weekLongClose=0;

        weekShortHigh=0;
        weekMediumHigh=0;
        weekLongHigh=0;

        weekShortLow=0;
        weekMediumLow=0;
        weekLongLow=0;

        weekBullishSignal=0;
        weekBearishSignal=0;
        weekBelow50Signal= 0;
        weekAbove50Signal=0;
        weekNeutralSignal=0;

        weekBullishZone=0;
        weekBearishZone=0;
        weekBelow50Zone=0;
        weekAbove50Zone=0;
        weekNeutralZone=0;
}

AddLabel(weekBullishZone, "W", color.GREEN); 
AddLabel(weekBearishZone, "W", color.RED); 
AddLabel(weekBelow50Zone, "W", color.PINK);
AddLabel(weekAbove50Zone, "W", color.LIGHT_GREEN);
AddLabel(weekNeutralZone, "W", color.YELLOW);


AssignPriceColor(
if      (coloredCandlesOn and weekBullishZone) then Color.GREEN 
else if (coloredCandlesOn and weekBearishZone) then Color.RED 
else if (coloredCandlesOn and weekBelow50Zone) then color.PINK
else if (coloredCandlesOn and weekAbove50Zone) then color.LIGHT_GREEN
else if (coloredCandlesOn and weekNeutralZone) then Color.YELLOW 
else Color.CURRENT);

# Daily averages

def dayShortClose;
def dayMediumClose;
def dayLongClose;

def dayShortHigh;
def dayMediumHigh;
def dayLongHigh;

def dayShortLow;
def dayMediumLow;
def dayLongLow;

plot dayBullishSignal;
plot dayBearishSignal;
plot dayBelow200Signal;
plot dayAbove200Signal;
plot dayNeutralSignal;

plot dayBullishZone;
plot dayBearishZone;
plot dayBelow200Zone;
plot dayAbove200Zone;
plot dayNeutralZone;


    if GetAggregationPeriod() <= AggregationPeriod.DAY{
        priceDayClose = close(period = "day");
        priceDayHigh = high(period = "day");
        priceDayLow = low(period = "day");
        
        dayShortClose = MovingAverage(averageType, priceDayClose, lengthShort);
        dayMediumClose = MovingAverage(averageType, priceDayClose, lengthMedium);
        dayLongClose = MovingAverage(averageType, priceDayClose, lengthLong);

        dayShortHigh = MovingAverage(averageType, priceDayHigh, lengthShort);
        dayMediumHigh = MovingAverage(averageType, priceDayHigh, lengthMedium);
        dayLongHigh = MovingAverage(averageType, priceDayHigh, lengthLong);

        dayShortLow = MovingAverage(averageType, priceDayLow, lengthShort);
        dayMediumLow = MovingAverage(averageType, priceDayLow, lengthMedium);
        dayLongLow = MovingAverage(averageType, priceDayLow, lengthLong);

        dayBullishSignal = priceDayLow > dayShortClose AND priceDayLow > dayMediumClose AND priceDayLow > dayLongClose;
        dayBearishSignal = priceDayHigh < dayShortClose AND priceDayHigh < dayMediumClose AND priceDayHigh < dayLongClose;    
        dayBelow200Signal = priceDayHigh < dayLongClose and !dayBearishSignal;
        dayAbove200Signal = priceDayLow > dayLongClose and !dayBullishSignal;
        dayNeutralSignal = !dayBullishSignal and !dayBearishSignal and !dayBelow200Signal and !dayAbove200Signal;

        dayBullishZone = dayBullishSignal;
        dayBearishZone = dayBearishSignal;
        dayBelow200Zone = dayBelow200Signal;
        dayAbove200Zone = dayAbove200Signal;
        dayNeutralZone = dayNeutralSignal;

    }
    else {

        priceDayClose = 0;
        priceDayHigh = 0;
        priceDayLow = 0;
    
        dayShortClose=0;
        dayMediumClose=0;
        dayLongClose=0;

        dayShortHigh=0;
        dayMediumHigh=0;
        dayLongHigh=0;

        dayShortLow=0;
        dayMediumLow=0;
        dayLongLow=0;

        dayBullishSignal=0;
        dayBearishSignal=0;
        dayBelow200Signal= 0;
        dayAbove200Signal=0;
        dayNeutralSignal=0;

        dayBullishZone=0;
        dayBearishZone=0;
        dayBelow200Zone=0;
        dayAbove200Zone=0;
        dayNeutralZone=0;
}

AddLabel(dayBullishZone, "D", color.GREEN); 
AddLabel(dayBearishZone, "D", color.RED); 
AddLabel(dayBelow200Zone, "D", color.PINK);
AddLabel(dayAbove200Zone, "D", color.LIGHT_GREEN);
AddLabel(dayNeutralZone, "D", color.YELLOW);


AssignPriceColor(
if      (coloredCandlesOn and dayBullishZone) then Color.GREEN 
else if (coloredCandlesOn and dayBearishZone) then Color.RED 
else if (coloredCandlesOn and dayBelow200Zone) then color.PINK
else if (coloredCandlesOn and dayAbove200Zone) then color.LIGHT_GREEN
else if (coloredCandlesOn and dayNeutralZone) then Color.YELLOW 
else Color.CURRENT);
