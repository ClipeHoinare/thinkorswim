script iff {
    input data = 0;
    input data2 = 0;
    input data3 = 0;
    def ret_val = if data then data2 else data3;
    plot return = ret_val;
}

script nz {
    input data = 0;
    input data2 = 0;
    def ret_val = if IsNaN(data) then 0 else data2;
    plot nz = ret_val;
}

script barssince {
    input data = close;
    input bars_total = 0;
    def ret_val = data from bars_total bars ago;
    plot return = ret_val;
}

script valuewhen {
    input data = 0;
    input data2 = 0;
    input data3 = 0;
    def ret_val = GetValue(data, data2, data3);
    plot return = ret_val;
}

script crossover {
    input data = 0;
    input data2 = 0;
    def ret_val = data crosses above data2;
    plot return = ret_val;
}

script ema {
    input data = 0;
    input data2 = 0;
    def ret_val = ExpAverage(data, data2);
    plot return = ret_val;
}

def na = Double.NaN;