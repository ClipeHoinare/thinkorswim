####digIntoThePast
#work in progress - not finished
# find if the stock was in the patern 1234
#       1 - close price above sma 10 and above sma 200
#       2 - close price below sma 10 and sma 10 below sma 200
#       3 - close price above sma 10 and above sma 200
#       4 - low and high prices around sma 200 (eventually yesterday high under sma 200 and today low  )


input daysToLookBackOne = 45; # we are using this for scanner to look for the ones that happen between (# days) and (# days + 10) ago
input daysToLookBackTwo = 25; # we are using this for scanner to look for the ones that happen between (# days) and (# days + 10) ago
input daysToLookBackThr = 15; # we are using this for scanner to look for the ones that happen between (# days) and (# days + 10) ago
def daysToLookBackFor = 0;    # we are using this for scanner to look for the ones that happen between (# days) and (# days + 10) ago
input lengthFast =  10;   # fast simple moving average
input lengthSlow = 200;   # slow moving average


input price = FundamentalType.CLOSE;
input aggregationPeriod = AggregationPeriod.DAY;
input averageType = AverageType.SIMPLE;

#def movAvg10 = MovingAverage(averageType, Fundamental(price, period = aggregationPeriod), lengthFast); 
#def movAvg20 = MovingAverage(averageType, Fundamental(price, period = aggregationPeriod), lengthSlow); 
def movAvg10 = DailySMA(price, aggregationPeriod, lengthFast, 0, no);
def movAvg20 = DailySMA(price, aggregationPeriod, lengthSlow, 0, no);

# what stage is the 
def bulletFlagZr1 = (((close[0] > movAvg10[0]) and (movAvg20[0] < movAvg10[0])));
def bulletFlagZr2 = (((close[0] < movAvg10[0]) and (movAvg20[0] > movAvg10[0])));

plot upFlag = if (bulletFlagZr1) then 1 else Double.NaN;
upFlag.SetDefaultColor(Color.YELLOW);
upFlag.SetPaintingStrategy(PaintingStrategy.VALUES_ABOVE);

plot dwFlag = if (bulletFlagZr2) then 2 else Double.NaN;
dwFlag.SetDefaultColor(Color.YELLOW);
dwFlag.SetPaintingStrategy(PaintingStrategy.VALUES_BELOW);

### check if the 1-2-3-4 is there
def bulletFlagOne = (((close[daysToLookBackOne] > movAvg10[daysToLookBackOne]) and (movAvg20[daysToLookBackOne] < movAvg10[daysToLookBackOne]))
        or ((close[daysToLookBackOne+1] > movAvg10[daysToLookBackOne+1]) and (movAvg20[daysToLookBackOne+1] < movAvg10[daysToLookBackOne+1]))
        or ((close[daysToLookBackOne+2] > movAvg10[daysToLookBackOne+2]) and (movAvg20[daysToLookBackOne+2] < movAvg10[daysToLookBackOne+2]))
        or ((close[daysToLookBackOne+3] > movAvg10[daysToLookBackOne+3]) and (movAvg20[daysToLookBackOne+3] < movAvg10[daysToLookBackOne+3]))
        or ((close[daysToLookBackOne+4] > movAvg10[daysToLookBackOne+4]) and (movAvg20[daysToLookBackOne+4] < movAvg10[daysToLookBackOne+4]))
        or ((close[daysToLookBackOne+5] > movAvg10[daysToLookBackOne+5]) and (movAvg20[daysToLookBackOne+5] < movAvg10[daysToLookBackOne+5]))
        or ((close[daysToLookBackOne+6] > movAvg10[daysToLookBackOne+6]) and (movAvg20[daysToLookBackOne+6] < movAvg10[daysToLookBackOne+6]))
        or ((close[daysToLookBackOne+7] > movAvg10[daysToLookBackOne+7]) and (movAvg20[daysToLookBackOne+7] < movAvg10[daysToLookBackOne+7]))
        or ((close[daysToLookBackOne+8] > movAvg10[daysToLookBackOne+8]) and (movAvg20[daysToLookBackOne+8] < movAvg10[daysToLookBackOne+8]))
        or ((close[daysToLookBackOne+9] > movAvg10[daysToLookBackOne+9]) and (movAvg20[daysToLookBackOne+9] < movAvg10[daysToLookBackOne+9]))
    );
def bulletFlagTwo = (((close[daysToLookBackTwo  ] < movAvg10[daysToLookBackTwo  ]) and (movAvg20[daysToLookBackTwo] > movAvg10[daysToLookBackTwo]))
        or ((close[daysToLookBackTwo+1] < movAvg10[daysToLookBackTwo+1]) and (movAvg20[daysToLookBackTwo+1] > movAvg10[daysToLookBackTwo+1]))
        or ((close[daysToLookBackTwo+2] < movAvg10[daysToLookBackTwo+2]) and (movAvg20[daysToLookBackTwo+2] > movAvg10[daysToLookBackTwo+2]))
        or ((close[daysToLookBackTwo+3] < movAvg10[daysToLookBackTwo+3]) and (movAvg20[daysToLookBackTwo+3] > movAvg10[daysToLookBackTwo+3]))
        or ((close[daysToLookBackTwo+4] < movAvg10[daysToLookBackTwo+4]) and (movAvg20[daysToLookBackTwo+4] > movAvg10[daysToLookBackTwo+4]))
        or ((close[daysToLookBackTwo+5] < movAvg10[daysToLookBackTwo+5]) and (movAvg20[daysToLookBackTwo+5] > movAvg10[daysToLookBackTwo+5]))
        or ((close[daysToLookBackTwo+6] < movAvg10[daysToLookBackTwo+6]) and (movAvg20[daysToLookBackTwo+6] > movAvg10[daysToLookBackTwo+6]))
        or ((close[daysToLookBackTwo+7] < movAvg10[daysToLookBackTwo+7]) and (movAvg20[daysToLookBackTwo+7] > movAvg10[daysToLookBackTwo+7]))
        or ((close[daysToLookBackTwo+8] < movAvg10[daysToLookBackTwo+8]) and (movAvg20[daysToLookBackTwo+8] > movAvg10[daysToLookBackTwo+8]))
        or ((close[daysToLookBackTwo+9] < movAvg10[daysToLookBackTwo+9]) and (movAvg20[daysToLookBackTwo+9] > movAvg10[daysToLookBackTwo+9]))
    );
def bulletFlagThr = (((close[daysToLookBackThr] > movAvg10[daysToLookBackThr]) and (movAvg20[daysToLookBackThr] < movAvg10[daysToLookBackThr]))
        or ((close[daysToLookBackThr+1] > movAvg10[daysToLookBackThr+1]) and (movAvg20[daysToLookBackThr+1] < movAvg10[daysToLookBackThr+1]))
        or ((close[daysToLookBackThr+2] > movAvg10[daysToLookBackThr+2]) and (movAvg20[daysToLookBackThr+2] < movAvg10[daysToLookBackThr+2]))
        or ((close[daysToLookBackThr+3] > movAvg10[daysToLookBackThr+3]) and (movAvg20[daysToLookBackThr+3] < movAvg10[daysToLookBackThr+3]))
        or ((close[daysToLookBackThr+4] > movAvg10[daysToLookBackThr+4]) and (movAvg20[daysToLookBackThr+4] < movAvg10[daysToLookBackThr+4]))
        or ((close[daysToLookBackThr+5] > movAvg10[daysToLookBackThr+5]) and (movAvg20[daysToLookBackThr+5] < movAvg10[daysToLookBackThr+5]))
        or ((close[daysToLookBackThr+6] > movAvg10[daysToLookBackThr+6]) and (movAvg20[daysToLookBackThr+6] < movAvg10[daysToLookBackThr+6]))
        or ((close[daysToLookBackThr+7] > movAvg10[daysToLookBackThr+7]) and (movAvg20[daysToLookBackThr+7] < movAvg10[daysToLookBackThr+7]))
        or ((close[daysToLookBackThr+8] > movAvg10[daysToLookBackThr+8]) and (movAvg20[daysToLookBackThr+8] < movAvg10[daysToLookBackThr+8]))
        or ((close[daysToLookBackThr+9] > movAvg10[daysToLookBackThr+9]) and (movAvg20[daysToLookBackThr+9] < movAvg10[daysToLookBackThr+9]))
    );

#TODO: current Flag is losing the days when current day low is higher than movAvg and yesterday high was lower that mowAvg, fix it in future
def bulletFlagFor = (((low[daysToLookBackFor] < movAvg20[daysToLookBackFor]) and (high[daysToLookBackFor] > movAvg20[daysToLookBackFor]))
        or ((low[daysToLookBackFor+1] < movAvg20[daysToLookBackFor+1]) and (high[daysToLookBackFor+1] > movAvg20[daysToLookBackFor+1]))
        or ((low[daysToLookBackFor+2] < movAvg20[daysToLookBackFor+2]) and (high[daysToLookBackFor+2] > movAvg20[daysToLookBackFor+2]))
        or ((low[daysToLookBackFor+3] < movAvg20[daysToLookBackFor+3]) and (high[daysToLookBackFor+3] > movAvg20[daysToLookBackFor+3]))
        or ((low[daysToLookBackFor+4] < movAvg20[daysToLookBackFor+4]) and (high[daysToLookBackFor+4] > movAvg20[daysToLookBackFor+4]))
        # or ((low[daysToLookBackFor+5] < movAvg20[daysToLookBackFor+5]))
        # or ((low[daysToLookBackFor+6] < movAvg20[daysToLookBackFor+6]))
        # or ((low[daysToLookBackFor+7] < movAvg20[daysToLookBackFor+7]))
        # or ((low[daysToLookBackFor+8] < movAvg20[daysToLookBackFor+8]))
        # or ((low[daysToLookBackFor+9] < movAvg20[daysToLookBackFor+9]))
    );

# def bulletFlagFiv = (((close[0] > movAvg20[0]))
        # or ((close[daysToLookBackFor+1] > movAvg20[daysToLookBackFor+1]))
        # or ((close[daysToLookBackFor+2] > movAvg20[daysToLookBackFor+2]))
        # or ((close[daysToLookBackFor+3] > movAvg20[daysToLookBackFor+3]))
        # or ((close[daysToLookBackFor+4] > movAvg20[daysToLookBackFor+4]))
        # or ((close[daysToLookBackFor+5] > movAvg20[daysToLookBackFor+5]))
        # or ((close[daysToLookBackFor+6] > movAvg20[daysToLookBackFor+6]))
        # or ((close[daysToLookBackFor+7] > movAvg20[daysToLookBackFor+7]))
        # or ((close[daysToLookBackFor+8] > movAvg20[daysToLookBackFor+8]))
        # or ((close[daysToLookBackFor+9] > movAvg20[daysToLookBackFor+9]))
    # );

#Plot arrows to flag the event
plot pivotDot = if (bulletFlagOne and bulletFlagTwo and bulletFlagThr and bulletFlagFor) then high + (22 * TickSize()) else Double.NaN;
pivotDot.SetPaintingStrategy(PaintingStrategy.POINTS);
pivotDot.SetLineWeight(5);
pivotDot.SetDefaultColor(Color.MAGENTA);


