## **README** ##

### **What is this repository for?** ###

* ThinkOrSwim scripts or other info


### **How do I get set up?** ###

* Use extendion .ts for the thickscript files
    - assign to CoffeeScript to .ts file for nice formating in Visual Studio Code

* Use extension .md for the info files

* Use a folder for each script/strategy 

### **Help** ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
