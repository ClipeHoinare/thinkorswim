#script exDivDate.ts
#using same procedure as earnDate.ts to do it for exDividend date
# inspiration from https://usethinkscript.com/threads/thinkorswim-earnings-date-indicator-and-watchlist-column.390/post-68783

def bn = BarNumber();
def na = Double.NaN;
def lastDividendBar = AbsValue(GetEventOffset(Events.DIVIDEND, 0));
def findMonth = GetMonth();
def findDay = GetDayOfMonth(GetYYYYMMDD());
def findYear = GetYear();

#ThinkScript thinks in numbers, including converting dates into numbers and viewing each bar on the chart as a number.  
#   Therefore you can use BarNumber to tell ThinkScript which bar
#   , and use lines in ThinkScript code to 'trick' ThinkScript into displaying that daily bar in date format
def lastDividendBarNumber = if !isNaN(lastDividendBar) then bn + lastDividendBar else na;
def nextDividendBar = bn == HighestAll(lastDividendBarNumber);
def lastDividendBarMonth = HighestAll(if nextDividendBar then findMonth else na);
def lastDividendBarDay = HighestAll(if nextDividendBar then findDay else na);
def lastDividendBarYear = HighestAll(if nextDividendBar then findYear else na);

# create a prety date good for a proper sorting
AddLabel(lastDividendBar >= 0
    , (if lastDividendBarMonth <= 9 and lastDividendBarMonth >=0 then "0" else "") + lastDividendBarMonth + "/" 
        + (if lastDividendBarDay <= 9 and lastDividendBarDay >=0 then "0" else "") + lastDividendBarDay + "/" 
        + AsPrice(lastDividendBarYear)
    , color.white);

AddLabel(isNaN(lastDividendBar), "  ", color.white);

#end script exDivDate.ts
