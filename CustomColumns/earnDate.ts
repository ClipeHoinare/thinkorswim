#script earnDate.ts
#https://usethinkscript.com/threads/thinkorswim-earnings-date-indicator-and-watchlist-column.390/post-68783
#Remarks
#Edited to keep only the date value, and format it for a good sorting 

# Next Earnings Date
# Paris
# 10.15.2019

##  IMPORTANT!!!! 44 EXPANSION You have to have enough expansion area on your chart available to reach the actual Earnings date, 
##      so if you have 0 expansion area, an inconsistent date will be displayed

#HINT: watchlist label that shows days-to-earnings and next earnings date (if next earnings date is available).  Includes a visual alert if "EARNINGS TODAY"

#def allows you to teach ThinkScript new "words" to save time and typing by using your new "words" later in the code
def bn = BarNumber();
def na = Double.NaN;
def nextEarningsBar = AbsValue(GetEventOffset(Events.EARNINGS, 0));
def findDay = GetDayOfMonth(GetYYYYMMDD());
def findMonth = GetMonth();
def findYear = GetYear();

#ThinkScript thinks in numbers, including converting dates into numbers and viewing each bar on the chart as a number. 
#   Therefore you can use BarNumber to tell ThinkScript which bar, 
#   and use lines in ThinkScript code to 'trick' ThinkScript into displaying that daily bar in date format
def nextEarningsBarNumber = if !isNaN(nextEarningsBar) then bn + nextEarningsBar else na;
def nextEarnings = bn == HighestAll(nextEarningsBarNumber);
def nextEarningsBarMonth = HighestAll(if nextEarnings then findMonth else na);
def nextEarningsBarDay = HighestAll(if nextEarnings then findDay else na);
def nextEarningsBarYear = HighestAll(if nextEarnings then findYear else na);

# create a prety date good for a proper sorting
AddLabel(nextEarningsBar > 0, 
    (if nextEarningsBarMonth <= 9 and nextEarningsBarMonth >=0 then "0" else "") + nextEarningsBarMonth + "/" 
        + (if nextEarningsBarDay <= 9 and nextEarningsBarDay >=0 then "0" else "") + nextEarningsBarDay + "/" 
        + AsPrice(nextEarningsBarYear),
    color.white);

#end script earnDate.ts
