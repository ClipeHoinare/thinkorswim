#script exDivNumb.ts
#using same procedure as earnNum.ts do it for exDividend date
# inspiration from https://usethinkscript.com/threads/earnings-dividend-and-p-e-labels-for-thinkorswim.1357/

def lastDividendBar = AbsValue(GetEventOffset(Events.DIVIDEND, 0));

addlabel(1
    ,   if isNaN(lastDividendBar) then " "
        else if lastDividendBar > 9 then "" + lastDividendBar
        else if lastDividendBar <= 9 and lastDividendBar >=0   then " " +  lastDividendBar
        else " "
    ,Color.white);

#end script exDivNumb.ts