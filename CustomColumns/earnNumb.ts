#earnNumb.ts
#follow ken on twitter @KRose_TDA
#ken.rose@tdameritrade.com  --thanks to mbox56 for helpful updates on this script.
#Script like this are available as part of 
#The Ken Rose thinkscripting webcast Tuesdays 5:30 ET
#Scan for stocks with earnings comming up within the week
# Link to Rose's custom column
#http://tos.mx/d9reMGz
#Updates
#09/01/21 - increased the days to 21 
#09/03/21 - replaced the "if else if" construction; no more limitation to 21 days(it looks like thinkorswim have some); 
#               keeping the space before the digit numbers as it works good for sort

def nextEarningsBar = AbsValue(GetEventOffset(Events.EARNINGS, 0));                  
def isAfter =   HasEarnings(EarningTime.AFTER_MARKET);
def isBefore = HasEarnings(EarningTime.BEFORE_MARKET)[-1];

addlabel(1
    ,   if isnan(nextEarningsBar) then " "
        else if nextEarningsBar > 9 then nextEarningsBar + " " + (if isBefore then "B" else if isAfter then "A" else "  ")
        else if nextEarningsBar <= 9 and nextEarningsBar >=0   then " " +  nextEarningsBar + " " + (if isBefore then "B" else if isAfter then "A" else "  ")
        else " "
    ,Color.white);

#end script earnNumb.ts